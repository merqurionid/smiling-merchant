package com.digiasia.smiling.merchant.utils.views;

import android.view.View;

import com.victor.loading.rotate.RotateLoading;

public class RotateLoadingHandler {
    private View frame;
    private RotateLoading loading;

    public RotateLoadingHandler(RotateLoading loading) {
        this.loading = loading;
    }

    public RotateLoadingHandler(View frame, RotateLoading loading) {
        this.frame = frame;
        this.loading = loading;
    }

    public void start() {
        if (frame != null) {
            frame.setVisibility(View.VISIBLE);
        }

        if (loading != null) {
            loading.setVisibility(View.VISIBLE);
            loading.start();
        }
    }

    public void stop() {
        if (frame != null) {
            frame.setVisibility(View.INVISIBLE);
        }

        if (loading != null) {
            loading.stop();
            loading.setVisibility(View.INVISIBLE);
        }
    }
}
