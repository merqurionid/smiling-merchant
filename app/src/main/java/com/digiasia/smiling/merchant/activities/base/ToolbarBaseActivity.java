package com.digiasia.smiling.merchant.activities.base;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.digiasia.smiling.merchant.R;
import com.digiasia.smiling.merchant.utils.Constants;
import com.digiasia.smiling.merchant.utils.Helpers;
import com.digiasia.smiling.merchant.utils.permission.PermissionRequest;

import butterknife.BindView;
import butterknife.ButterKnife;

public abstract class ToolbarBaseActivity extends AppCompatActivity implements Constants {
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.main_title) TextView title;

    private PermissionRequest permissionRequest;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayout());
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        setDarkStatusBar();

        Helpers.setupToolbar(this, toolbar, title, false);

        setupPermissionRequest();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void hideTitle() {
        toolbar.setVisibility(View.GONE);
    }

    public void setLightStatusBar() {
        Helpers.setLightStatusBar(this, R.color.background_1);
    }

    public void setDarkStatusBar() {
        Helpers.setDarkStatusBar(this);
    }

    private void setupPermissionRequest() {
        permissionRequest = PermissionRequest.getInstance();

        permissionRequest.addPermissions(PermissionRequest.PERMISSION_CAMERA_REQUEST,
                PermissionRequest.PERMISSION_READ_REQUEST);

        permissionRequest.init(this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        permissionRequest.onRequestPermissionResult(requestCode, grantResults);
    }

    public void setTitle(String sTitle) {
        title.setText(sTitle);
    }

    public void fixTitlePosition() {
        Helpers.fixTitlePosition(title);
    }

    public void fixTitlePosition(int dp) {
        Helpers.fixTitlePosition(title, dp);
    }

    public void fixTitlePositionActionBar() {
        fixTitlePosition(-20);
    }

    public void setHomeAsUp(boolean homeAsUp) {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(homeAsUp);

        if (!homeAsUp) {
            fixTitlePosition();
        } else {
            fixTitlePosition(Constants.SAFE_TOOLBAR_PADDING_RIGHT_DP);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    protected abstract @LayoutRes
    int getLayout();
}
