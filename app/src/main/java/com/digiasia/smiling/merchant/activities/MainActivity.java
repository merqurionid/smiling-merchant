package com.digiasia.smiling.merchant.activities;

import android.arch.lifecycle.ViewModelProviders;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import com.digiasia.smiling.merchant.R;
import com.digiasia.smiling.merchant.activities.base.MainBaseActivity;
import com.digiasia.smiling.merchant.fragments.MenuHistoryFragment;
import com.digiasia.smiling.merchant.fragments.MenuScanFragment;
import com.digiasia.smiling.merchant.utils.Helpers;
import com.digiasia.smiling.merchant.viewmodels.MainViewModel;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends MainBaseActivity implements BottomNavigationView.OnNavigationItemSelectedListener {
    @BindView(R.id.main_bottom_nav) BottomNavigationView navigation;
    @BindView(R.id.homebar_saldo_value) TextView tvSaldo;

    private MainViewModel mViewModel;
    private MenuScanFragment menuScanFragment;
    private MenuHistoryFragment menuHistoryFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        initFragment();
        navigation.setOnNavigationItemSelectedListener(this);
        navigation.setSelectedItemId(R.id.navigation_scan);
        navigation.setItemIconTintList(null);

        mViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        mViewModel.init(this);
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_main;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        invalidateOptionsMenu();
        super.fixTitlePosition();

        switch (menuItem.getItemId()) {
            case R.id.navigation_scan:
                loadFragment(menuScanFragment);
                showHomebar();
                break;
            case R.id.navigation_history:
                loadFragment(menuHistoryFragment);
                loadFragment(menuHistoryFragment);
                setTitle(getString(R.string.title_history));
                showCommonbar();
                break;
        }

        return true;
    }

    @Override
    public void onBackPressed() {
        if (navigation.getSelectedItemId() == R.id.navigation_scan) {
            finish();
        } else {
            navigation.setSelectedItemId(R.id.navigation_scan);
        }
    }

    private void initFragment() {
        menuScanFragment = new MenuScanFragment();
        menuHistoryFragment = new MenuHistoryFragment();
    }

    private void loadFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.main_frame_container, fragment);
        transaction.commitAllowingStateLoss();
    }

    private void gotoLogin() {
        Helpers.gotoAndFinishActivity(this, LoginActivity.class);
    }
}
