package com.digiasia.smiling.merchant.fragments;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.digiasia.smiling.merchant.R;
import com.digiasia.smiling.merchant.models.Booking;
import com.digiasia.smiling.merchant.viewmodels.MenuHistoryFragmentViewModel;

import java.util.List;

import butterknife.ButterKnife;

public class MenuHistoryFragment extends Fragment {

    private MenuHistoryFragmentViewModel mViewModel;

    public MenuHistoryFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_menu_history, container, false);
        ButterKnife.bind(this, view);

        mViewModel = ViewModelProviders.of(this).get(MenuHistoryFragmentViewModel.class);
        mViewModel.init(getActivity());

        mViewModel.getBooking().observe(this, bookings -> {
            setRV(bookings);
        });

        mViewModel.getIsLoadingBooking().observe(this, isLoading -> {
            // TODO doSomething when loading is true
        });

        // InitRV

        return view;
    }

    private void initRV() {
        // TODO init recylerview, set adapter, etc
    }

    private void setRV(List<Booking> list) {
        // TODO set List<Booking> to recyclerview, remember to check if null
    }
}
