package com.digiasia.smiling.merchant.models;

public class Booking {

    private String title;
    private String photo;
    // TODO Tambahan field, ini masih asumsi

    public Booking() {
        title = "";
        photo = "";
    }

    public Booking(String title, String photo) {
        this.title = title;
        this.photo = photo;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
