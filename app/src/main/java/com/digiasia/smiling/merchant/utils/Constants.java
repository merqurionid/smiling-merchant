package com.digiasia.smiling.merchant.utils;

public interface Constants {
    String FILE_PROVIDER                = "com.digiasia.smiling.fileprovider";

    int SAFE_TOOLBAR_PADDING_RIGHT_DP   = 70;
    int NETWORK_DUMMY_DELAY_TIME        = 1000;
}
