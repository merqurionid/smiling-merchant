package com.digiasia.smiling.merchant.repositories;

import android.app.Activity;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;

import com.digiasia.smiling.merchant.models.User;
import com.digiasia.smiling.merchant.utils.Helpers;
import com.digiasia.smiling.merchant.utils.Preferences;
import com.google.gson.Gson;

public class UserRepository {
    private static UserRepository instance;

    private User user = new User();

    public static UserRepository getInstance(Activity activity) {
        if (instance == null) {
            instance = new UserRepository();
        }
        return instance;
    }

    public MutableLiveData<User> getUser(Context context) {
        MutableLiveData<User> data = new MutableLiveData<>();

        String dataPref = Preferences.getInstance(context).getString(Preferences.LOGIN_DATA);
        if (!Helpers.isEmptyString(dataPref)) {
            user = new Gson().fromJson(dataPref, User.class);
        }

        data.setValue(user);
        return data;
    }

    public void setUser(Context context, String id, String name, String username, String address, String phone, String photo, String type, int saldo) {
        user = new User();
        user.setId(id);
        user.setName(name);
        user.setUsername(username);
        user.setAddress(address);
        user.setPhone(phone);
        user.setPhoto(photo);
        user.setType(type);
        user.setSaldo(saldo);

        Preferences.getInstance(context).putString(Preferences.LOGIN_DATA, new Gson().toJson(user));
    }

    public void setUser(Context context, String username) {
        setUser(context,"111", username, username, "Bandung", "081234567890", "http://i.pravatar.cc/150?u=sm_"+username, "ASN", 2000000);
    }

    public void removeUserData(Activity activity) {
        user = null;
        Preferences.getInstance(activity).putString(Preferences.LOGIN_DATA, "");
    }
}
