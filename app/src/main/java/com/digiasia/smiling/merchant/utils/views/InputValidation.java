package com.digiasia.smiling.merchant.utils.views;

import android.app.Activity;
import android.databinding.Observable;
import android.databinding.ObservableField;
import android.support.annotation.IdRes;
import android.support.annotation.StringRes;
import android.support.design.widget.TextInputLayout;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.regex.Pattern;

public class InputValidation {
    private ArrayList<Validation> validations;

    public InputValidation() {
        validations = new ArrayList<>();
    }

    public void addValidation(Activity activity, @IdRes int view, String regex, @StringRes int text) {
        TextInputLayout ttl = activity.findViewById(view);
        String info = activity.getString(text);

        validations.add(new Validation(view, ttl, regex, info));
    }

    public void addFieldValidation(Activity activity, ObservableField<String> field, @IdRes final int inputLayoutId, String regex, @StringRes int errorText) {
        addValidation(activity, inputLayoutId, regex, errorText);

        field.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable sender, int propertyId) {
                validate(inputLayoutId);
            }
        });
    }

    public boolean isValid() {
        boolean isValid;
        int count = 0;

        do {
            isValid = validations.get(count).isValid();
            count++;
        } while (validations.size() > count && isValid);

        return isValid;
    }

    public void validate() {
        for (Validation val : validations) {
            val.setErrorView();
        }
    }

    public void validate(@IdRes int view) {
        for (Validation val : validations) {
            if (val.getId() == view) {
                val.setErrorView();
            }
        }
    }

    class Validation {
        private int id;
        private TextInputLayout til;
        private String regex;
        private String text;

        public Validation(@IdRes int view, TextInputLayout til, String regex, String text) {
            this.id = view;
            this.til = til;
            this.regex = regex;
            this.text = text;
        }

        public int getId() {
            return id;
        }

        public EditText getEditText() {
            return til.getEditText();
        }

        public String getRegex() {
            return regex;
        }

        public String getText() {
            return text;
        }

        public boolean isValid() {
            String trim = getEditText().getText().toString().replaceAll("\n", "");
            return Pattern.compile(getRegex()).matcher(trim).matches();
        }

        public void setErrorView() {
            if (!isValid()) {
                setError();
            } else {
                resetError();
            }
        }

        public void setError() {
            til.setError(getText());
        }

        public void resetError() {
            til.setError(null);
        }
    }


}
