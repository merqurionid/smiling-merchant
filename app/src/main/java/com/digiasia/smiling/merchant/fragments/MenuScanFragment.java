package com.digiasia.smiling.merchant.fragments;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.digiasia.smiling.merchant.R;
import com.digiasia.smiling.merchant.activities.LoginActivity;
import com.digiasia.smiling.merchant.models.User;
import com.digiasia.smiling.merchant.utils.Helpers;
import com.digiasia.smiling.merchant.viewmodels.MenuScanFragmentViewModel;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MenuScanFragment extends Fragment {
    @BindView(R.id.section_user_username) TextView tvUserName;
    @BindView(R.id.section_user_usertype) TextView tvUserType;
    @BindView(R.id.section_user_userphoto) ImageView ivUserPhoto;

    private MenuScanFragmentViewModel mViewModel;

    public MenuScanFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_menu_scan, container, false);
        ButterKnife.bind(this, view);

        mViewModel = ViewModelProviders.of(this).get(MenuScanFragmentViewModel.class);
        mViewModel.init(getActivity());

        mViewModel.getUser().observe(this, user -> {
            if (user != null && !Helpers.isEmptyString(user.getId())) {
                setUserView(user);
            } else {
                gotoLogin();
            }
        });

        return view;
    }

    private void setUserView(User user) {
        if (user != null && !Helpers.isEmptyString(user.getId())) {

            tvUserName.setText(user.getUsername());
            tvUserType.setText(user.getType());

            Glide.with(getActivity()).load(user.getPhoto())
                    .apply(Helpers.roundedTransform())
                    .placeholder(R.drawable.pl_no_user)
                    .into(ivUserPhoto);
        }
    }

    @OnClick(R.id.section_user_logout)
    public void doLogout() {
        mViewModel.doLogout(getActivity());
    }

    private void gotoLogin() {
        Helpers.gotoAndFinishActivity(getActivity(), LoginActivity.class);
    }
}
