package com.digiasia.smiling.merchant.models;

public class User {
    private String id;
    private String username;
    private String name;
    private String photo;
    private String phone;
    private String address;
    private String type;
    private String gender;
    private int saldo;

    public User() {}

    public User(String id, String username, String name, String photo, String phone, String address, String type, String gender, int saldo) {
        this.id = id;
        this.username = username;
        this.name = name;
        this.photo = photo;
        this.phone = phone;
        this.address = address;
        this.type = type;
        this.gender = gender;
        this.saldo = saldo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public float getSaldo() {
        return saldo;
    }

    public void setSaldo(int saldo) {
        this.saldo = saldo;
    }
}
