package com.digiasia.smiling.merchant.utils.permission;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import java.util.HashMap;

public class PermissionRequest {
    public static final int PERMISSION_CAMERA_REQUEST   = 1;
    public static final int PERMISSION_READ_REQUEST     = 2;
    public static final int PERMISSION_WRITE_REQUEST    = 3;

    private HashMap<Integer, Permission> permissions = new HashMap<>();

    public static PermissionRequest instance;

    public static PermissionRequest getInstance() {
        if (instance == null) {
            instance = new PermissionRequest();
        }
        return instance;
    }

    public PermissionRequest() {
    }

    public void addPermissions(int... ids) {
        for (int id: ids) {
            addPermission(id);
        }
    }

    private void addPermission(int id) {
        Permission p = null;
        switch (id) {
            case PERMISSION_CAMERA_REQUEST:
                p = new Permission(id, Manifest.permission.CAMERA, false);
                break;
            case PERMISSION_READ_REQUEST:
                p = new Permission(id, Manifest.permission.READ_EXTERNAL_STORAGE, false);
                break;
            case PERMISSION_WRITE_REQUEST:
                p = new Permission(id, Manifest.permission.WRITE_EXTERNAL_STORAGE, false);
                break;
        }

        if (p != null) permissions.put(id, p);
    }

    public void init(Context context) {
        for (Permission p: permissions.values()) {
            p.setGranted(ContextCompat.checkSelfPermission(context, p.name) == PackageManager.PERMISSION_GRANTED);
        }
    }

    public void requestPermission(Activity activity, int id) {
        permissions.get(id).requestPermission(activity);
    }

    public void onRequestPermissionResult(int requestCode, int[] grantResults) {
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            permissions.get(requestCode).setGranted(true);
        }
    }

    public boolean isGranted(int id) {
        return permissions.get(id).isGranted();
    }

    class Permission {
        private int id;
        private String name;
        private boolean isGranted;

        public Permission(int id, String name, boolean isGranted) {
            this.id = id;
            this.name = name;
            this.isGranted = isGranted;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public boolean isGranted() {
            return isGranted;
        }

        public void setGranted(boolean granted) {
            isGranted = granted;
        }

        public void requestPermission(Activity activity) {
            if (ContextCompat.checkSelfPermission(activity, name) == PackageManager.PERMISSION_GRANTED) {
                isGranted = true;
            } else {
                isGranted = false;
                ActivityCompat.requestPermissions(activity, new String[]{name}, id);
            }
        }
    }
}

