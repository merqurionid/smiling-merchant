package com.digiasia.smiling.merchant.viewmodels;

import android.app.Activity;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.content.Context;
import android.databinding.ObservableField;
import android.os.AsyncTask;

import com.digiasia.smiling.merchant.models.User;
import com.digiasia.smiling.merchant.repositories.UserRepository;
import com.digiasia.smiling.merchant.utils.Constants;

public class LoginViewModel extends ViewModel {

    private MutableLiveData<User> mUser;
    private MutableLiveData<Boolean> mIsProcessing = new MutableLiveData<>();
    private MutableLiveData<String> mErrorMessage = new MutableLiveData<>();
    private UserRepository mRepo;

    private ObservableField<String> username = new ObservableField<>();
    private ObservableField<String> password = new ObservableField<>();

    private Activity activity;

    public void init(Activity activity) {

        if (mUser != null) {
            return;
        }

        this.activity = activity;

        mRepo = UserRepository.getInstance(activity);
        mUser = mRepo.getUser(activity);

        mIsProcessing.setValue(false);
        mErrorMessage.setValue(null);

        username.set("Victor Manu");
        password.set("123456");
    }

    public void doLogin(Context context) {
        mIsProcessing.setValue(true);

        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... voids) {

                try {
                    Thread.sleep(Constants.NETWORK_DUMMY_DELAY_TIME);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);

                mIsProcessing.setValue(false);

                if (password.get().equals("123456")) {
                    mErrorMessage.setValue(null);

                    mRepo.setUser(context, username.get());

                    mUser.postValue(mRepo.getUser(context).getValue());

                } else {
                    mErrorMessage.setValue("Maaf, username dan password tidak cocok");
                }
            }
        }.execute();
    }

    public MutableLiveData<User> getUser() {
        return mUser;
    }

    public MutableLiveData<Boolean> getIsProcessing() {
        return mIsProcessing;
    }

    public MutableLiveData<String> getErrorMessage() {
        return mErrorMessage;
    }

    public ObservableField<String> getUsername() {
        return username;
    }

    public ObservableField<String> getPassword() {
        return password;
    }

    public UserRepository getUserRepo() {
        return mRepo;
    }
}
