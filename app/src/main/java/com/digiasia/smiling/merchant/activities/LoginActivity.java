package com.digiasia.smiling.merchant.activities;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import com.digiasia.smiling.merchant.R;
import com.digiasia.smiling.merchant.databinding.ActivityLoginBinding;
import com.digiasia.smiling.merchant.utils.Helpers;
import com.digiasia.smiling.merchant.utils.views.InputValidation;
import com.digiasia.smiling.merchant.utils.views.RotateLoadingHandler;
import com.digiasia.smiling.merchant.viewmodels.LoginViewModel;

public class LoginActivity extends AppCompatActivity {

    private LoginViewModel mLoginViewModel;
    private InputValidation validation = new InputValidation();
    private RotateLoadingHandler loadingHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ActivityLoginBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_login);

        mLoginViewModel = ViewModelProviders.of(this).get(LoginViewModel.class);
        mLoginViewModel.init(this);

        binding.setViewModel(mLoginViewModel);

        Helpers.setLightStatusBar(this);

        mLoginViewModel.getUser().observe(this, user -> {
            if (user != null && !Helpers.isEmptyString(user.getId())) {
                gotoMain();
            }
        });

        mLoginViewModel.getErrorMessage().observe(this, message -> {
            if (message != null) {
                Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
            }
        });

        loadingHandler = new RotateLoadingHandler(binding.loading);

        mLoginViewModel.getIsProcessing().observe(this, isProcessing -> {
            if (isProcessing) {
                loadingHandler.start();
            } else {
                loadingHandler.stop();
            }
        });

        validation.addFieldValidation(this, mLoginViewModel.getUsername(), R.id.login_il_username, Helpers.patternMinLength(5), R.string.error_required_5_chars);
        validation.addFieldValidation(this, mLoginViewModel.getPassword(), R.id.login_il_password, Helpers.patternMinLength(5), R.string.error_required_5_chars);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (!mLoginViewModel.getIsProcessing().getValue())
            return super.dispatchTouchEvent(ev);
        return true;
    }

    public void submit(View view) {
        validation.validate();

        if (validation.isValid()) mLoginViewModel.doLogin(this);
    }

    private void gotoMain() {
        Helpers.gotoAndFinishActivity(this, MainActivity.class);
    }
}
