package com.digiasia.smiling.merchant.viewmodels;

import android.app.Activity;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.digiasia.smiling.merchant.models.User;
import com.digiasia.smiling.merchant.repositories.UserRepository;

public class MainViewModel extends ViewModel {

    private MutableLiveData<User> mUser;
    private UserRepository mRepo;

    public void init(Activity activity) {

        if (mUser != null) {
            return;
        }
        mRepo = UserRepository.getInstance(activity);
        mUser = mRepo.getUser(activity);
    }

    public void doLogout(Activity activity) {
        mRepo.removeUserData(activity);
        mUser.setValue(null);
    }

    public MutableLiveData<User> getUser() {
        return mUser;
    }
}
