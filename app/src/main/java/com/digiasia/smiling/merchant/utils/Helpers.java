package com.digiasia.smiling.merchant.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.request.RequestOptions;
import com.digiasia.smiling.merchant.R;
import com.jaeger.library.StatusBarUtil;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import jp.wasabeef.glide.transformations.RoundedCornersTransformation;

public class Helpers {
    private static final int MIUI = 1;
    private static final int FLYME = 2;
    private static final int ANDROID_M = 3;
    private static final Locale INDONESIAN_LOCALE = new Locale("in", "ID");

    public static boolean isEmptyString(String input) {
        return input == null || input.equals("") || input.trim().equals("") || input.equals("null");
    }

    public static String toTitleCase(String input) {
        StringBuilder titleCase = new StringBuilder();
        boolean nextTitleCase = true;

        for (char c : input.toCharArray()) {
            if (Character.isSpaceChar(c)) {
                nextTitleCase = true;
            } else if (nextTitleCase) {
                c = Character.toTitleCase(c);
                nextTitleCase = false;
            }

            titleCase.append(c);
        }

        return titleCase.toString();
    }

    public static String toCapitalize(String input) {
        return Character.toUpperCase(input.charAt(0)) + input.substring(1);
    }

    public static String toMoneyFormat(double input) {
        return String.format(INDONESIAN_LOCALE, "%,.0f", input);
    }

    public static String toCurrencyFormat(double input) {
        return "Rp. " + toMoneyFormat(input);
    }

    private static final NavigableMap<Long, String> suffixes = new TreeMap<>();
    static {
        suffixes.put(1_000L, "k");
        suffixes.put(1_000_000L, "m");
        suffixes.put(1_000_000_000L, "g");
        suffixes.put(1_000_000_000_000L, "t");
        suffixes.put(1_000_000_000_000_000L, "p");
        suffixes.put(1_000_000_000_000_000_000L, "e");
    }

    public static String toShortNumberFormat(long value) {
        //Long.MIN_VALUE == -Long.MIN_VALUE so we need an adjustment here
        if (value == Long.MIN_VALUE) return toShortNumberFormat(Long.MIN_VALUE + 1);
        if (value < 0) return "-" + toShortNumberFormat(-value);
        if (value < 1000) return Long.toString(value); // easy case

        Map.Entry<Long, String> e = suffixes.floorEntry(value);
        Long divideBy = e.getKey();
        String suffix = e.getValue();

        long truncated = value / (divideBy / 10); // the number part of the output times 10
        boolean hasDecimal = truncated < 100 && (truncated / 10d) != (truncated / 10);
        return hasDecimal ? (truncated / 10d) + suffix : (truncated / 10) + suffix;
    }

    public static String getCommonDateFormatted(Date date) {
        return getDateFormatted(date, "d MMMM yyyy");
    }

    public static String getSummaryDateFormatted(Date date) {
        return getDateFormatted(date, "MMMM yyyy");
    }

    public static String getEditTextDateFormatted(Date date) {
        return getDateFormatted(date, "d/MM/yyyy");
    }

    public static String getCommonTimeFormatted(Date date) {
        return getDateFormatted(date, "hh:mm a");
    }

    public static String getStopwatchTimeFormatted(long milliseconds) {
        int seconds = (int) (milliseconds / 1000) % 60 ;
        int minutes = (int) ((milliseconds / (1000*60)) % 60);
        int hours   = (int) ((milliseconds / (1000*60*60)) % 24);

        return String.format("%02d : %02d : %02d", hours, minutes, seconds);
    }

    public static String getDateFormatted(Date date, String pattern) {
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        return date != null ? format.format(date) : "";
    }

    public static Calendar getCalendar(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        return calendar;
    }

    public static Calendar getCalendar(int year, int monthOfYear, int dayOfMonth, boolean dateType) {
        Calendar calendar = Calendar.getInstance();
        if (dateType) {
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, monthOfYear);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        } else {
            calendar.set(Calendar.HOUR_OF_DAY, year);
            calendar.set(Calendar.MINUTE, monthOfYear);
            calendar.set(Calendar.SECOND, dayOfMonth);
        }

        return calendar;
    }

    public static Date getDate(int year, int monthOfYear, int dayOfMonth) {
        return getCalendar(year, monthOfYear, dayOfMonth, true).getTime();
    }

    public static String patternMinLength(int length) {
        return ".{" + length + ",}";
    }

    public static String patternFirstString(String match) {
        return "^["+match+"]{"+match.length()+"}";
    }

    public static boolean stringMatchRegex(String string, String regex) {
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(string);
        return m.find();
    }

    public static void setLightStatusBar(Activity activity, int color, int alpha) {
        StatusBarUtil.setLightMode(activity);
        StatusBarUtil.setTransparent(activity);
        StatusBarUtil.setColor(activity, activity.getResources().getColor(color), alpha);
    }

    public static void setLightStatusBar(Activity activity, int color) {
        setLightStatusBar(activity, color, 0);
    }

    public static void setLightStatusBar(Activity activity) {
        setLightStatusBar(activity, R.color.background_2, 0);
    }

    public static void setDarkStatusBar(Activity activity, int color, int alpha) {
        StatusBarUtil.setDarkMode(activity);
        StatusBarUtil.setTransparent(activity);
        StatusBarUtil.setColor(activity, activity.getResources().getColor(color), alpha);
    }

    public static void setDarkStatusBar(Activity activity, int color) {
        setDarkStatusBar(activity, color, 0);
    }

    public static void setDarkStatusBar(Activity activity) {
        setDarkStatusBar(activity, R.color.primary_color, 0);
    }

    public static void setupToolbar(AppCompatActivity activity, Toolbar toolbar, TextView title, boolean homeAsUp) {
        activity.setSupportActionBar(toolbar);
        ActionBar actionBar = activity.getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(homeAsUp);
        if (!homeAsUp) {
            fixTitlePosition(title);
        } else {
            Helpers.fixTitlePosition(title, Constants.SAFE_TOOLBAR_PADDING_RIGHT_DP);
        }
    }

    public static void fixTitlePosition(TextView textView) {
        fixTitlePosition(textView, 20);
    }

    public static void fixTitlePosition(TextView textView, int dp) {
        textView.setPadding(0, 0, dpToPx(textView.getContext(), dp), 0);
    }

    public static int dpToPx(Context context, float dp) {
        return (int) (dp * context.getResources().getDisplayMetrics().density + 0.5f);
    }

    public static void gotoActivity(Context context, Class<?> cls, Bundle extras) {
        Intent intent = new Intent(context, cls);
        if (extras != null) intent.putExtras(extras);

        context.startActivity(intent);
    }

    public static void gotoActivity(Context context, Class<?> cls) {
        gotoActivity(context, cls, null);
    }

    public static void gotoAndFinishActivity(Activity activity, Class<?> cls, Bundle extras) {
        gotoActivity(activity, cls, extras);
        activity.finish();
    }

    public static void gotoAndFinishActivity(Activity activity, Class<?> cls) {
        gotoAndFinishActivity(activity, cls, null);
    }

    public static void gotoActivityForResult(Activity activity, Class<?> cls, Bundle extras, int requestCode) {
        Intent intent = new Intent(activity, cls);
        if (extras != null) intent.putExtras(extras);

        activity.startActivityForResult(intent, requestCode);
    }

    public static void gotoActivityForResult(Activity activity, Class<?> cls, int requestCode) {
        gotoActivityForResult(activity, cls, null, requestCode);
    }

    public static DatePickerDialog makeDatePicker(DatePickerDialog.OnDateSetListener callBack, Calendar minDate, Calendar maxDate, Calendar calendar, String color) {
        DatePickerDialog dpd = DatePickerDialog.newInstance(callBack,
                calendar.get(Calendar.YEAR), // Initial year selection
                calendar.get(Calendar.MONTH), // Initial month selection
                calendar.get(Calendar.DAY_OF_MONTH) // Inital day selection
        );
        dpd.setAccentColor(color);
        dpd.setMinDate(minDate);
        dpd.setMaxDate(maxDate);
        return dpd;
    }

    public static DatePickerDialog makeDatePicker(DatePickerDialog.OnDateSetListener callBack, Calendar minDate, Calendar maxDate, Date value) {
        Calendar defMinDate = Calendar.getInstance();
        defMinDate.add(Calendar.YEAR, -1);

        Calendar defMaxDate = Calendar.getInstance();
        defMaxDate.add(Calendar.YEAR, 1);

        Calendar cal = (value == null) ? Calendar.getInstance() : Helpers.getCalendar(value);
        minDate = (minDate == null) ? defMinDate : minDate;
        maxDate = (maxDate == null) ? defMaxDate : maxDate;

        return makeDatePicker(callBack, minDate, maxDate, cal,"#111111");
    }

    public static void setFont(Context context, TextView view, int font) {
        view.setTypeface(ResourcesCompat.getFont(context, font));
    }

    public static void fixFont(Context context, TextView view) {
        setFont(context, view, R.font.opensans_regular);
    }

    public static RequestOptions roundedTransform(int radius) {
        return new RequestOptions().transforms(new CenterCrop(), new RoundedCornersTransformation(radius, 0,
                RoundedCornersTransformation.CornerType.ALL));
    }

    public static RequestOptions roundedTransform() {
        return roundedTransform(45);
    }
}

