package com.digiasia.smiling.merchant.repositories;

import android.arch.lifecycle.MutableLiveData;

import com.digiasia.smiling.merchant.models.Booking;

import java.util.ArrayList;
import java.util.List;

public class BookingRepository {
    private static BookingRepository instance;

    private ArrayList<Booking> dataSet = new ArrayList<>();

    public static BookingRepository getInstance() {
        if (instance == null) {
            instance = new BookingRepository();
        }
        return instance;
    }

    public MutableLiveData<List<Booking>> getBookings() {
        MutableLiveData<List<Booking>> data = new MutableLiveData<>();
        data.setValue(dataSet);

        return data;
    }

    public void setDataDummy() {
        dataSet = new ArrayList<>();

        dataSet.add(new Booking(
            "Rafting Cisadane Bogor",
            "http://www.dpa-adventure.com/wp-content/uploads/2018/02/Rafting-Bogor-4.jpg"
        ));

        dataSet.add(new Booking(
            "Kawah Putih",
            "http://anekatempatwisata.com/wp-content/uploads/2017/07/Kawah-Putih-Bandung.jpg"
        ));

        dataSet.add(new Booking(
            "Stone Garden",
            "http://anekatempatwisata.com/wp-content/uploads/2017/07/Stone-Garden-Padalarang-768x430.jpg"
        ));

        dataSet.add(new Booking(
            "Green Canyon Pangandaran",
            "http://anekatempatwisata.com/wp-content/uploads/2017/07/Green-Canyon-Pangandaran.jpg"
        ));

        dataSet.add(new Booking(
            "Curug Cikaso",
            "http://anekatempatwisata.com/wp-content/uploads/2017/07/Curug-Cikaso-Sukabumi.jpg"
        ));

        dataSet.add(new Booking(
            "Alam Panenjoan",
            "http://anekatempatwisata.com/wp-content/uploads/2017/07/Alam-Panenjoan-Purwakarta.jpg"
        ));

        dataSet.add(new Booking(
            "Bukit Panembongan",
            "http://anekatempatwisata.com/wp-content/uploads/2017/07/Bukit-Panembongan-Kuningan.jpg"
        ));

        dataSet.add(new Booking(
            "Kampung Karuhun",
            "http://anekatempatwisata.com/wp-content/uploads/2017/07/Kampung-Karuhun-Sumedang.jpg"
        ));

        dataSet.add(new Booking(
            "D\'Jungle Private Camp Bogor",
            "http://anekatempatwisata.com/wp-content/uploads/2017/07/D%E2%80%99Jungle-Private-Camp-Puncak-Bogor.jpg"
        ));

        dataSet.add(new Booking(
            "Pantai Ujung Genteng",
            "http://anekatempatwisata.com/wp-content/uploads/2017/07/Pantai-Ujung-Genteng-Sukabumi.jpg"
        ));

    }

}
