package com.digiasia.smiling.merchant.viewmodels;

import android.app.Activity;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.digiasia.smiling.merchant.models.Booking;
import com.digiasia.smiling.merchant.repositories.BookingRepository;

import java.util.List;

public class MenuHistoryFragmentViewModel extends ViewModel {

    private MutableLiveData<List<Booking>> mBooking = new MutableLiveData<>();
    private MutableLiveData<Boolean> mIsLoadingBooking = new MutableLiveData<>();

    private BookingRepository mBookingRepo;

    public void init(Activity activity) {

        mBookingRepo = BookingRepository.getInstance();
        mBooking = mBookingRepo.getBookings();
        mIsLoadingBooking.setValue(false);

    }

    public MutableLiveData<List<Booking>> getBooking() {
        return mBooking;
    }

    public MutableLiveData<Boolean> getIsLoadingBooking() {
        return mIsLoadingBooking;
    }
}
